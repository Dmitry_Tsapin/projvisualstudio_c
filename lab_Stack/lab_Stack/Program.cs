﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Stack
{
    class Program
    {
        static void Main(string[] args)
        {
 //         Stack<int> x = new Stack<int>(); Типизированный стек
            Stack x = new Stack();
            x.Push("Москва");
            x.Push(123);
            x.Push("Санкт-Петербург");
            x.Push(4);
            x.Push(8);
            //
            Console.WriteLine(x.Peek());
            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());
            Console.WriteLine(x.Pop());
          

            #region
            try
            {
                Console.WriteLine(x.Pop());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка:" + ex.Message);
            }
           #endregion
        }
    }
}
