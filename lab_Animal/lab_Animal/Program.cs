﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Animal
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat x1;
            x1 = new Cat();
            x1.Name = "Барсик";
            Console.WriteLine(x1.Name);
            Console.WriteLine(x1.Legs);
            Console.WriteLine("-----");

            // пример с котом
            Animal x;
            x = new Cat();
            Console.WriteLine(x.Legs);
            Cat xx = (Cat)x;
            Console.WriteLine(xx.Name);

            // пример с собакой
            Animal x3;
            x3 = new Dog();
            Console.WriteLine(x3.Legs);
            Dog xxx = (Dog)x3;
            Console.WriteLine(xxx.Name);


            // пример с человеком
            Human x5;
            x5 = new Student();
            Console.WriteLine(x5.money);
            Student aaa = (Student)x5;
            Console.WriteLine(aaa.name);
        }
    }
}
