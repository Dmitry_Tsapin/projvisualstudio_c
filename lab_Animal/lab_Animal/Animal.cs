﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Animal
{
    class Animal
    {
        public int Legs { get; protected set; }
    }
    class Pet : Animal
    {
        public int Fleas { get; protected set; }
        public string Name { get; set; }
        public Pet()
        {
            Legs = 4;
            Name = "Питомец";
        }
    }
    class Cat: Pet
    {
        public Cat()
        {
            Name = "Мурзик";
        }
    }
    class Dog : Pet
    {
        public Dog()
        {
            Name = "Барбос";
        }
    }

    class Human : Animal
    {
        public Human()
        {
            Legs = 4;
           
        }
        public string name { get; set; }
        public string money { get; set; }
    }

    class Student : Human
    {
        public Student()
        {
            name = "Дмитрий";
            money = "1500";
        }
      
    }

}
