﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_move_box
{
    public partial class Form1 : Form
    {
        int speed;
        public Form1()
        {
            InitializeComponent();
            speed = 0;
            this.KeyDown += Form1_KeyDown;
            this.KeyUp += Form1_KeyUp;

            timer1.Tick += Timer1_Tick;
            progressBar1.Value = 100;
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            MonstrMove(pictureBox2, 4);
            MonstrMove(pictureBox3, 3);
            MonstrMove(pictureBox4, 5);

            if (progressBar1.Value == 0)
            {
                timer1.Enabled = false;
                MessageBox.Show("Вы проиграли!");
                
            }
        }

        private void MonstrMove(object sender, int speed)
        {
            PictureBox p = (PictureBox)sender;
            
            int xt = p.Location.X;
            int yt = p.Location.Y;
            if (xt > pictureBox1.Location.X)
            {
                xt = xt - speed;
                
            }
            else
            {
                xt = xt + speed;
            }

            if (yt > pictureBox1.Location.Y)
            {
                yt = yt - speed;
            }
            else
            {
                yt = yt + speed;
            }
            p.Location = new Point(xt, yt);

            if (xt == pictureBox1.Location.X)
            {
                progressBar1.Value -= 10;
            }

            if (yt == pictureBox1.Location.Y)
            {
                progressBar1.Value -= 10;
            }
            
            
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            speed = 1;
            laspeed.Text = speed.ToString();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                int x = pictureBox1.Location.X;
                int y = pictureBox1.Location.Y;
                pictureBox1.Location = new Point(pictureBox1.Location.X - speed, pictureBox1.Location.Y);
                if (x - speed > 0)
                {
                    pictureBox1.Location = new Point(x - speed, y);
                }
                else
                {
                    pictureBox1.Location = new Point(0, y);
                }
            }

            if (e.KeyCode == Keys.Right && pictureBox1.Location.X + speed + pictureBox1.Width < this.Width)
            {
                
                pictureBox1.Location = new Point(pictureBox1.Location.X + speed, pictureBox1.Location.Y);
                
            }
        
            if (e.KeyCode == Keys.Up && pictureBox1.Location.Y + speed + pictureBox1.Height > 0)
            {
                pictureBox1.Location = new Point(pictureBox1.Location.X ,  pictureBox1.Location.Y - speed);
            }

            if (e.KeyCode == Keys.Down && pictureBox1.Location.Y + speed + pictureBox1.Height < this.Height)
            {
                pictureBox1.Location = new Point(pictureBox1.Location.X, pictureBox1.Location.Y + speed);
            }
            laspeed.Text = speed.ToString();
            speed++;
        }
        
       
        

        
    }
}
