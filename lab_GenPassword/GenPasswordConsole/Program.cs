﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab_GenPasswordCore;

namespace GenPasswordConsole
{
    class Program
    {
         static bool check()
        {
            bool res;
            string boll = Console.ReadLine();
            if (boll == "да")
                res = true;
            else
                res = false;
            return res;

        }
        
        static int lenght()
        {
            int res = 0;
            string boll = Console.ReadLine();
            int.TryParse(boll, out res);
            return res;
        }

      


        static void Main(string[] args)
        {
            
            Console.WriteLine("Добро пожаловать в генератор!");
            Console.WriteLine("1.Сгенерировать пароль символами верхнего регистра?");
            bool chUp = check();
            Console.WriteLine("2.Сгенерировать пароль символами нижнего регистра?");
            bool chLower = check();
            Console.WriteLine("3.Сгенерировать пароль цифрами?");
            bool chNumber = check();
            Console.WriteLine("4.Сгенерировать пароль специальными символами?");
            bool chSpec = check();
            Console.WriteLine("5.Сгенерировать пароль русскими буквами?");
            bool chRus = check();
            Console.WriteLine("6.Введите длину пароля");
            int edLenght = lenght();
            Console.WriteLine("7.Вывести пароль");
            Console.WriteLine(Utils.RandomStr((int)edLenght, chLower,chUp, chNumber, chSpec, chRus));



        }
    }
}
