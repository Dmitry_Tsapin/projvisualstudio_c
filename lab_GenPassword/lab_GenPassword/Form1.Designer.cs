﻿namespace lab_GenPassword
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edPassword = new System.Windows.Forms.TextBox();
            this.buGen = new System.Windows.Forms.Button();
            this.chLower = new System.Windows.Forms.CheckBox();
            this.chUp = new System.Windows.Forms.CheckBox();
            this.chNumber = new System.Windows.Forms.CheckBox();
            this.chSpec = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.edLenght = new System.Windows.Forms.NumericUpDown();
            this.chRus = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.edLenght)).BeginInit();
            this.SuspendLayout();
            // 
            // edPassword
            // 
            this.edPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edPassword.Location = new System.Drawing.Point(190, 48);
            this.edPassword.Multiline = true;
            this.edPassword.Name = "edPassword";
            this.edPassword.Size = new System.Drawing.Size(378, 51);
            this.edPassword.TabIndex = 0;
            this.edPassword.Text = "<Password>";
            this.edPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buGen
            // 
            this.buGen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buGen.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buGen.ForeColor = System.Drawing.Color.Red;
            this.buGen.Location = new System.Drawing.Point(190, 120);
            this.buGen.Name = "buGen";
            this.buGen.Size = new System.Drawing.Size(378, 63);
            this.buGen.TabIndex = 1;
            this.buGen.Text = "Генерировать";
            this.buGen.UseVisualStyleBackColor = true;
            // 
            // chLower
            // 
            this.chLower.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chLower.AutoSize = true;
            this.chLower.Location = new System.Drawing.Point(190, 216);
            this.chLower.Name = "chLower";
            this.chLower.Size = new System.Drawing.Size(217, 21);
            this.chLower.TabIndex = 2;
            this.chLower.Text = "Символы в нижнем регистре";
            this.chLower.UseVisualStyleBackColor = true;
            // 
            // chUp
            // 
            this.chUp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chUp.AutoSize = true;
            this.chUp.Location = new System.Drawing.Point(190, 252);
            this.chUp.Name = "chUp";
            this.chUp.Size = new System.Drawing.Size(221, 21);
            this.chUp.TabIndex = 3;
            this.chUp.Text = "Символы в верхнем регистре";
            this.chUp.UseVisualStyleBackColor = true;
            // 
            // chNumber
            // 
            this.chNumber.AutoSize = true;
            this.chNumber.Location = new System.Drawing.Point(190, 296);
            this.chNumber.Name = "chNumber";
            this.chNumber.Size = new System.Drawing.Size(78, 21);
            this.chNumber.TabIndex = 4;
            this.chNumber.Text = "Цифры";
            this.chNumber.UseVisualStyleBackColor = true;
            // 
            // chSpec
            // 
            this.chSpec.AutoSize = true;
            this.chSpec.Location = new System.Drawing.Point(190, 339);
            this.chSpec.Name = "chSpec";
            this.chSpec.Size = new System.Drawing.Size(181, 21);
            this.chSpec.TabIndex = 5;
            this.chSpec.Text = "Специальные символы";
            this.chSpec.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(187, 390);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Длина пароля";
            // 
            // edLenght
            // 
            this.edLenght.Location = new System.Drawing.Point(296, 388);
            this.edLenght.Name = "edLenght";
            this.edLenght.Size = new System.Drawing.Size(120, 22);
            this.edLenght.TabIndex = 7;
            // 
            // chRus
            // 
            this.chRus.AutoSize = true;
            this.chRus.Location = new System.Drawing.Point(469, 216);
            this.chRus.Name = "chRus";
            this.chRus.Size = new System.Drawing.Size(144, 21);
            this.chRus.TabIndex = 8;
            this.chRus.Text = "Русские символы";
            this.chRus.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.chRus);
            this.Controls.Add(this.edLenght);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chSpec);
            this.Controls.Add(this.chNumber);
            this.Controls.Add(this.chUp);
            this.Controls.Add(this.chLower);
            this.Controls.Add(this.buGen);
            this.Controls.Add(this.edPassword);
            this.MinimumSize = new System.Drawing.Size(818, 497);
            this.Name = "Form1";
            this.Text = "Gen_Password";
            ((System.ComponentModel.ISupportInitialize)(this.edLenght)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edPassword;
        private System.Windows.Forms.Button buGen;
        private System.Windows.Forms.CheckBox chLower;
        private System.Windows.Forms.CheckBox chUp;
        private System.Windows.Forms.CheckBox chNumber;
        private System.Windows.Forms.CheckBox chSpec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown edLenght;
        private System.Windows.Forms.CheckBox chRus;
    }
}

