﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab_GenPasswordCore;
namespace lab_GenPassword
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //
            buGen.Click += BuGen_Click;
        }

        private void BuGen_Click(object sender, EventArgs e)
        {
            edPassword.Text = Utils.RandomStr((int)edLenght.Value, chLower.Checked,
                chUp.Checked, chNumber.Checked, chSpec.Checked, chRus.Checked);
        }
    }
}
