﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_GenPasswordCore
{
    public static class Utils
    {
        public static string RandomStr(int edLenght, bool chLower, bool chUp, bool chNumber, bool chSpec, bool chRus)
        {
            string c1 = "abcdefghijklmnopqrstuvwxyz";
            string c2 = "0123456789";
            string c3 = "[]{}<>,.;:-+#";
            string c4 = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            //
            var x = new StringBuilder(); // набор символов
            var xResult = new StringBuilder();
            Random rnd = new Random();
            //Создаем набор символов в соответсвии с параметрами функции
            if (chLower) x.Append(c1);
            if (chUp & !chRus & chLower) x.Append(c1.ToUpper());
            if (chNumber) x.Append(c2);
            if (chSpec) x.Append(c3);
            if (chRus) x.Append(c4);
            if (chRus & chUp & !chLower) x.Append(c4);
            if (chUp & chRus & chLower)
            {
                x.Append(c1.ToUpper());
                x.Append(c4.ToUpper());
            }

            if (chUp & !chRus & !chLower)
            {
                x.Append(c1.ToUpper());
                x.Append(c4.ToUpper());
            }
            // Если набор остался пустым то по умолчанию включаем символы нижнего регистра
            if (x.ToString() == "") x.Append(c1);
            //
            while (xResult.Length < edLenght)
                xResult.Append(x[rnd.Next(x.Length)]);
            //
            return xResult.ToString();
        }
    }
}
