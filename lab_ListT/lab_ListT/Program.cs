﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_ListT
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> x = new List<int>() { 1, 2, 3, 45 };
            x.Add(6);
            x.AddRange(new int[] { 7, 8, 9 });
            x.Insert(0, 500);
            x.RemoveAt(1);
            Console.WriteLine("Сумма:", x.Sum());
            x.Reverse(1, 2);
            Console.WriteLine("----------------");
            //
            foreach (int i in x)
            {
                Console.WriteLine(i);
            }
        }
    }
}
