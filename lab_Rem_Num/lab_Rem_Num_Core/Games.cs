﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_Rem_Num_Core
{
    public class Games
    {
        private Timer tm;

        public int CountCorrect { get; protected set; }
        public int CountWrong { get; protected set; }
        public int Code { get; protected set; }
        public int Points { get; protected set; }
        public int Level { get; protected set; }

        public event EventHandler GoScreenRemember;
        public event EventHandler GoScreenAnswer;

        public Games()
        {
            tm = new Timer();
            tm.Enabled = false;
            tm.Interval = 2000;
            tm.Tick += Tm_Tick;
        }

        private void Tm_Tick(object sender, EventArgs e)
        {
            tm.Enabled = false;
            if (GoScreenAnswer != null)
                GoScreenAnswer(this, EventArgs.Empty);
        }

        private int getMin()
        {
            switch (Level)
            {
                case 1:
                    return 1000;
                    break;
                case 2:
                    return 10000;
                    break;
                case 3:
                    return 100000;
                    break;
                default: return 100000;
            }
        }


        private int getMax()
        {
            switch (Level)
            {
                case 1:
                    return 1000;
                    break;
                case 2:
                    return 10000;
                    break;
                case 3:
                    return 100000;
                    break;
                default: return 100000;
            }
        }

        public void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            DoContinue();
        }

        public void DoContinue()
        {
            Random rnd = new Random();
            Code = rnd.Next(100000, 999999);
            tm.Enabled = true;
            //
            if (GoScreenRemember != null)
                GoScreenRemember(this, EventArgs.Empty);

        }

        public void DoAnswer(int v)
        {
            if (v == Code)
            {
                Points++;
                CountCorrect++;
                tm.Interval -= 500;
                Level++;
            }


            else
            {
                Points--;
                CountWrong++;
                Level--;
                tm.Interval += 500;
            }

            DoContinue();
        }
    }
}

