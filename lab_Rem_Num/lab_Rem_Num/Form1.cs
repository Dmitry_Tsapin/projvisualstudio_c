﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab_Rem_Num_Core;

namespace lab_Rem_Num
{
    
    public partial class Form1 : Form
    {
        private Games g;
        public Form1()
        {
            InitializeComponent();
            g = new Games();
            g.GoScreenRemember += G_GoScreenRemember;
            g.GoScreenAnswer += G_GoScreenAnswer;
            g.DoReset();
            //
            buAnswer.Click += BuAnswer_Click;
        }

        private void BuAnswer_Click(object sender, EventArgs e)
        {
            int x;
            int.TryParse(edAnswer.Text, out x);
            g.DoAnswer(x);
        }

        private void G_GoScreenAnswer(object sender, EventArgs e)
        {
            edAnswer.Text = "";
            laRemember.Visible = false;
            paRemember.Visible = false;
            laAnswer.Visible = true;
            edAnswer.Visible = true;
            buAnswer.Visible = true;
        }

        private void G_GoScreenRemember(object sender, EventArgs e)
        {
            laCorrect.Text = String.Format("Верно = {0}", g.CountCorrect.ToString());
            laWrong.Text = String.Format("Неверно = {0}", g.CountWrong.ToString());
            laPoints.Text = String.Format("Очки = {0}", g.Points.ToString());
            laLevel.Text = String.Format("Уровень = {0}", g.Level.ToString());
            laCode.Text = g.Code.ToString();
            laRemember.Visible = true;
            paRemember.Visible = true;
            laAnswer.Visible = false;
            edAnswer.Visible = false;
            buAnswer.Visible = false;
        }
    }
}
