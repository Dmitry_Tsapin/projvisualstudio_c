﻿namespace lab_Rem_Num
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.laCorrect = new System.Windows.Forms.Label();
            this.laWrong = new System.Windows.Forms.Label();
            this.laRemember = new System.Windows.Forms.Label();
            this.paRemember = new System.Windows.Forms.Panel();
            this.laCode = new System.Windows.Forms.Label();
            this.laAnswer = new System.Windows.Forms.Label();
            this.edAnswer = new System.Windows.Forms.TextBox();
            this.buAnswer = new System.Windows.Forms.Button();
            this.laPoints = new System.Windows.Forms.Label();
            this.laLevel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.paRemember.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.laCorrect, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.laWrong, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(389, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // laCorrect
            // 
            this.laCorrect.AutoSize = true;
            this.laCorrect.BackColor = System.Drawing.Color.Lime;
            this.laCorrect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laCorrect.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laCorrect.Location = new System.Drawing.Point(3, 0);
            this.laCorrect.Name = "laCorrect";
            this.laCorrect.Size = new System.Drawing.Size(188, 100);
            this.laCorrect.TabIndex = 0;
            this.laCorrect.Text = "Верно:";
            // 
            // laWrong
            // 
            this.laWrong.AutoSize = true;
            this.laWrong.BackColor = System.Drawing.Color.Red;
            this.laWrong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.laWrong.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laWrong.Location = new System.Drawing.Point(197, 0);
            this.laWrong.Name = "laWrong";
            this.laWrong.Size = new System.Drawing.Size(189, 100);
            this.laWrong.TabIndex = 1;
            this.laWrong.Text = "Неверно:";
            // 
            // laRemember
            // 
            this.laRemember.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laRemember.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laRemember.Location = new System.Drawing.Point(9, 181);
            this.laRemember.Name = "laRemember";
            this.laRemember.Size = new System.Drawing.Size(389, 29);
            this.laRemember.TabIndex = 1;
            this.laRemember.Text = "Запомни число";
            this.laRemember.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // paRemember
            // 
            this.paRemember.Controls.Add(this.laCode);
            this.paRemember.Location = new System.Drawing.Point(3, 222);
            this.paRemember.Name = "paRemember";
            this.paRemember.Size = new System.Drawing.Size(398, 89);
            this.paRemember.TabIndex = 2;
            // 
            // laCode
            // 
            this.laCode.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laCode.Location = new System.Drawing.Point(9, 29);
            this.laCode.Name = "laCode";
            this.laCode.Size = new System.Drawing.Size(386, 42);
            this.laCode.TabIndex = 0;
            this.laCode.Text = "0000000";
            this.laCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laAnswer
            // 
            this.laAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laAnswer.Location = new System.Drawing.Point(9, 327);
            this.laAnswer.Name = "laAnswer";
            this.laAnswer.Size = new System.Drawing.Size(389, 29);
            this.laAnswer.TabIndex = 3;
            this.laAnswer.Text = "Введите число";
            this.laAnswer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // edAnswer
            // 
            this.edAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.edAnswer.Location = new System.Drawing.Point(0, 374);
            this.edAnswer.Multiline = true;
            this.edAnswer.Name = "edAnswer";
            this.edAnswer.Size = new System.Drawing.Size(398, 49);
            this.edAnswer.TabIndex = 4;
            this.edAnswer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buAnswer
            // 
            this.buAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buAnswer.Location = new System.Drawing.Point(3, 429);
            this.buAnswer.Name = "buAnswer";
            this.buAnswer.Size = new System.Drawing.Size(395, 48);
            this.buAnswer.TabIndex = 5;
            this.buAnswer.Text = "Готово";
            this.buAnswer.UseVisualStyleBackColor = true;
            // 
            // laPoints
            // 
            this.laPoints.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laPoints.Location = new System.Drawing.Point(7, 124);
            this.laPoints.Name = "laPoints";
            this.laPoints.Size = new System.Drawing.Size(211, 39);
            this.laPoints.TabIndex = 6;
            this.laPoints.Text = "Очки:";
            this.laPoints.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // laLevel
            // 
            this.laLevel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.laLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laLevel.Location = new System.Drawing.Point(177, 124);
            this.laLevel.Name = "laLevel";
            this.laLevel.Size = new System.Drawing.Size(211, 39);
            this.laLevel.TabIndex = 7;
            this.laLevel.Text = "Уровень:";
            this.laLevel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 482);
            this.Controls.Add(this.laLevel);
            this.Controls.Add(this.laPoints);
            this.Controls.Add(this.buAnswer);
            this.Controls.Add(this.edAnswer);
            this.Controls.Add(this.laAnswer);
            this.Controls.Add(this.paRemember);
            this.Controls.Add(this.laRemember);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(418, 529);
            this.Name = "Form1";
            this.Text = "Rem num";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.paRemember.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label laCorrect;
        private System.Windows.Forms.Label laWrong;
        private System.Windows.Forms.Label laRemember;
        private System.Windows.Forms.Panel paRemember;
        private System.Windows.Forms.Label laCode;
        private System.Windows.Forms.Label laAnswer;
        private System.Windows.Forms.TextBox edAnswer;
        private System.Windows.Forms.Button buAnswer;
        private System.Windows.Forms.Label laPoints;
        private System.Windows.Forms.Label laLevel;
    }
}

