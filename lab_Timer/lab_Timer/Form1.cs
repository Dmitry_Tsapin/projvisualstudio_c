﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_Timer
{
    public partial class Form1 : Form
    {
        Random rnd;
        int speed;
        public Form1()
        {
            InitializeComponent();
            timer1.Tick += Timer1_Tick;
            rnd = new Random();
            speed = rnd.Next(15) + 2;
        }
        
        private void Timer1_Tick(object sender, EventArgs e)
        {
            pictureBox1.Location = new Point(0,pictureBox1.Location.Y + speed);
            if (pictureBox1.Location.Y > this.Size.Height)
            {
                pictureBox1.Location = new Point(0, 0);
                speed = rnd.Next(15) + 2;
            }

            pictureBox2.Location = new Point(pictureBox2.Location.X, pictureBox2.Location.Y + speed);
            if (pictureBox2.Location.Y > this.Size.Height - pictureBox2.Height)
            {
                pictureBox2.Location = new Point(50, 0);
                speed = rnd.Next(15) + 2;
            }
            pictureBox3.Location = new Point(pictureBox3.Location.X, pictureBox3.Location.Y + speed);
            if (pictureBox3.Location.Y > this.Size.Height - pictureBox3.Height)
            {
                pictureBox3.Location = new Point(120, 0);
            }
            pictureBox4.Location = new Point(pictureBox4.Location.X, pictureBox4.Location.Y + speed);
            if (pictureBox4.Location.Y > this.Size.Height - pictureBox4.Height)
            {
                pictureBox4.Location = new Point(280, 0);
                speed = rnd.Next(15) + 2;
            }
            

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            int score;
            Int32.TryParse(lscore.Text, out score);
            score++;
            lscore.Text = score.ToString();
            pictureBox1.Location = new Point(0, 0);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            int score;
            Int32.TryParse(lscore.Text, out score);
            score++;
            lscore.Text = score.ToString();
            pictureBox3.Location = new Point(280, 0);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            int score;
            Int32.TryParse(lscore.Text, out score);
            score++;
            lscore.Text = score.ToString();
            pictureBox2.Location = new Point(50, 0);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            int score;
            Int32.TryParse(lscore.Text, out score);
            score++;
            lscore.Text = score.ToString();
            pictureBox3.Location = new Point(120, 0);
        }
    }
}
