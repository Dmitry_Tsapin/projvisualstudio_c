﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Threading;


namespace lab_Trainer_Account
{
    class Games
    {
        public int CountCorrect { get; protected set; }
        public int CountIncorrect { get; protected set; }
        public int Level { get; protected set; }
        public int Points { get; protected set; }
        public bool AnswerCorrect { get; protected set; }
        public string CodeText { get; protected set; }

        public event EventHandler Change;

        public void DoReset()
        {
            CountCorrect = 0;
            CountIncorrect = 0;
            Level = 0;
            Points = 0;
            DoContinue();
        }

        public void DoContinue()
        {
            Random rnd = new Random();
            int xValue1 = 0;
            int xValue2 = 0;
            if (Level < 6)
            {
                xValue1 = rnd.Next(20);
                xValue2 = rnd.Next(20);
            }

            else if (Level < 12)
            {
                xValue1 = rnd.Next(80);
                xValue2 = rnd.Next(80);
            }

             else if (Level < 18)
            {
                xValue1 = rnd.Next(140);
                xValue2 = rnd.Next(140);
            }

            else if (Level < 24)
            {
                xValue1 = rnd.Next(200);
                xValue2 = rnd.Next(200);
            }

            else if (Level < 30)
            {
                xValue1 = rnd.Next(300);
                xValue2 = rnd.Next(300);
            }

            else if (Level < 36)
            {
                xValue1 = rnd.Next(400);
                xValue2 = rnd.Next(400);
            }

            int xResult = xValue1 + xValue2;
            int xResultNew;
            int xSign;
            if (rnd.Next(2) == 1)
            {
                xResultNew = xResult;
            }
            else
            {
                if (rnd.Next(2) == 1)
                    xSign = 1;
                else
                    xSign = -1;
                xResultNew = xResult + (rnd.Next(7) * xSign);
            }
            AnswerCorrect = (xResult == xResultNew);
            CodeText = String.Format("{0} + {1} = {2}", xValue1, xValue2, xResultNew);
            //
            if (Change != null)
                Change(this, EventArgs.Empty);


        }




        public void DoAnswer(bool v)
        {
            if (v == AnswerCorrect)
            {
                CountCorrect++;
                Level++;
                Points++;

            }
            else if (Points < 1)
            {
                DoReset();

            }

            else if (Level == 40)
            {
                MessageBox.Show("Поздравляем! Вы прошли игру ");
                DoReset();
            }

            else
            {
                CountIncorrect++;
                Points--;
                Level--;

            }
       
            DoContinue();
        }
    }

}
