﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_Trainer_Account
{
    public partial class Fm : Form
    {
        private Games g;

        public Fm()
        {
            InitializeComponent();

            g = new Games();
            g.Change += Event_Change;
            g.DoReset();
        }

        private void Event_Change(object sender, EventArgs e)
        {
            laCorrect.Text = String.Format("Верно = {0}", g.CountCorrect.ToString());
            laIncorrect.Text = String.Format("Неверно = {0}", g.CountIncorrect.ToString());
            laLevel.Text = String.Format("Уровень = {0}  ", g.Level.ToString());
            laPoints.Text = String.Format("Количество очков = {0}", g.Points.ToString());
            laCode.Text = g.CodeText;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void buYes_Click(object sender, EventArgs e)
        {
            g.DoAnswer(true);
        }

        private void buNo_Click(object sender, EventArgs e)
        {
            g.DoAnswer(false);
        }

        private void buReset_Click(object sender, EventArgs e)
        {
            g.DoReset();
        }
    }
}
