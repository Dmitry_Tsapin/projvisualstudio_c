﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_LinkList
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<string> x = new LinkedList<string>();

            x.AddLast("Вкусная");
            x.AddFirst("Самая");
            x.AddAfter(x.Last, "Шаурма у Ашота");
            x.AddBefore(x.First, "Эй,друг!");
           
            //
            foreach (string i in x)
            {
                Console.WriteLine(i);
            }
        }
    }
}
