﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Poezia
{
    static class Utils_Str
    {
        // через строчку
        public static string[] ThroughLine(string[] v)
        {
            var x = new StringBuilder();
            string[] xResult = new string[v.Length];
            for (int i = 0; i < v.Length; i++)
            {
                x.Clear();
                x.Append(v[i]);
                if (i % 2 ==1)
                {
                    for (int j = 0; j < x.Length; j++)
                    {
                        if (x[j] != ' ')
                            x[j] = 'x';
                    }
                }
                xResult[i] = x.ToString();
            }
            return xResult;

        }
        // первые слова
        public static string[] FirstWords(string[] v)
        {
            var x = new StringBuilder();
            string[] xResult = new string[v.Length];
            bool xFlag;
            for (int i = 0; i < v.Length; i++)
            {
                x.Clear();
                x.Append(v[i]);
                xFlag = false;
                for (int j = 0; j < x.Length; j++)
                {
                    if ((xFlag) && (x[j] != ' '))
                        x[j] = 'x';
                    if ((!xFlag) && (x[j] == ' '))
                        xFlag = true;
                }
                xResult[i] = x.ToString();
            }
            return xResult;
        }

        // строки слитно
        public static string[] FirstWordsX(string[] v)
        {
            var x = new StringBuilder();
            string[] xResult = new string[v.Length];
            bool xFlag;
            for (int i = 0; i < v.Length; i++)
            {
                x.Clear();
                x.Append(v[i]);
                xFlag = false;
                for (int j = 0; j < x.Length; j++)
                {
                    if ((xFlag) && (x[j] != ' '))
                        xFlag = true;
                    if ((!xFlag) && (x[j] == ' '))
                        x[j] = 'x';
                   

                }
                xResult[i] = x.ToString();
            }
            return xResult;
        }


        // каждая четная строка слитно
        public static string[] Cross(string[] v)
        {
            var x = new StringBuilder();
            string[] xResult = new string[v.Length];
            for (int i = 0; i < v.Length; i++)
            {
                x.Clear();
                x.Append(v[i]);
                if (i % 2 == 1)
                {
                    for (int j = 0; j < x.Length; j++)
                    {
                        if (x[j] == ' ')
                            x[j] = 'x';
                    }
                }
                xResult[i] = x.ToString();
            }
            return xResult;

        }
    }
}
