﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_Poezia
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            tabControl1.TabIndex = 0;
            textBox1.Lines = Utils_Str.ThroughLine(textBox2.Lines);
            textBox3.Lines = Utils_Str.FirstWords(textBox2.Lines);
            meFirstWords_X.Lines = Utils_Str.FirstWordsX(textBox2.Lines);
            textBox4.Lines = Utils_Str.Cross(textBox2.Lines);
            buZoomOut.Click += buZoomOut_Click;
            buZoomIn.Click += buZoomIn_Click;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("labPoezia - Dmitry Tsapin");
        }

        private void buZoomOut_Click(object sender, EventArgs e)
        {
            float x = textBox2.Font.Size;
            x -= 1;
            textBox2.Font = new Font(textBox2.Font.FontFamily, x);
            textBox1.Font = new Font(textBox1.Font.FontFamily, x);
            textBox3.Font = new Font(textBox3.Font.FontFamily, x);
        }

        private void buZoomIn_Click(object sender, EventArgs e)
        {
            float x = textBox2.Font.Size;
            x += 1;
            textBox2.Font = new Font(textBox2.Font.FontFamily, x);
            textBox1.Font = new Font(textBox1.Font.FontFamily, x);
            textBox3.Font = new Font(textBox3.Font.FontFamily, x);
        }
    }
}
