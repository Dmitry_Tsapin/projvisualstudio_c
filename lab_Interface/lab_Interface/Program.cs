﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            IAge x = new Interface();
            x.ShowAge();
            x.InsertAge();

            IFIO y = (IFIO)x;
            y.ShowFIO();

            IStep z = (IStep)x;
            z.ShowStep();
            z.InsertStep();
        }
    }
}
