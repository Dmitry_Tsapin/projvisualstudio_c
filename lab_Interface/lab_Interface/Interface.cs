﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Interface
{
    interface IAge
    {
        void ShowAge();
        void InsertAge();
    }

    interface IFIO
    {
        void ShowFIO();
    }

    interface IStep
    {
        void ShowStep();
        void InsertStep();
    }

    class Interface : IAge, IFIO, IStep
    {
        public void ShowAge()
        {
            Console.WriteLine("Возраст");
        }

        public void InsertAge()
        {
            int x;
            Console.Write("Введите возраст");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Ваш возраст:", x);
        }

        public void ShowFIO()
        {
            int z;
            Console.WriteLine("ФИО");
            
        }

        public void ShowStep()
        {
            Console.WriteLine("Стипендия");
        }

        public void InsertStep()
        {
            int x;
            Console.Write("Введите стипендию");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Ваша стипендия:", x);
        }
    }
}
