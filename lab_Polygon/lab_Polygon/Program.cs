﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Polygon
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Polygon> x = new List<Polygon>();
            x.Add(new Polygon());
            x.Add(new Rectangle());
            x.Add(new Triangle());
            x.Add(new Square());
            //
            
            foreach (Polygon i in x)
            {
                i.Draw();
            }
            // оператор as
            Polygon m = new Square();
            Square b = m as Square;
            if (b != null)
            {
                Console.WriteLine(b.ToString());
            }
            // оператор is
            if (m is Square)
            {
                Console.WriteLine("m is Square");
            }
            else
                Console.WriteLine("m is not Square");
        }
    }
}
