﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Polygon
{
    class Polygon
    {
        public virtual void Draw()
        {
            Console.WriteLine("Polygon.Draw");
        }
    }

    class Rectangle : Polygon
    {
        public override void Draw()
        {
            Console.WriteLine("Rectangke.Draw");
        }
    }

    class Triangle : Polygon
    {
        public override void Draw()
        {
            Console.WriteLine("Triangle.Draw");
        }
    }

    class Square : Polygon
    {
        public override void Draw()
        {
            Console.WriteLine("Square.Draw");
        }
    }
}
