﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Rectangle
{
    class Program
    {
        static void Main(string[] args)
        {  
            // пример без параметров
            Rectangle x = new Rectangle();
            Console.WriteLine(x.GetArea());

            //пример с передачей параметров
            Rectangle x1 = new Rectangle(2,3);
            Console.WriteLine(x1.GetArea());

            //пример с передачей параметров (периметр прямоугольника)
            Rectangle x3 = new Rectangle(4, 5);
            Console.WriteLine(x3.GetPerimetr());

            // пример с квадратом (новый созданный класс)
            Sqaure x2 = new Sqaure(4);
            Console.WriteLine(x2.Square());


        }
    }
}
