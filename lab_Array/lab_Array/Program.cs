﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Array
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] x = { 1, 2, 3, 4, 5 };
            Console.WriteLine(x[1]);
            Console.WriteLine(x[3] + x[2]);

            string[] y = { "1", "2", "3", "4" };
            Console.WriteLine(y[1]);
            Console.WriteLine(y[3] + y[2]);

        }
    }
}
