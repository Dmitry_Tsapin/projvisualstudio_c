﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Snake_Game
{
    public partial class Form1 : Form
    {
        private List<Circ> Snake = new List<Circ>();
        private Circ food = new Circ();
        private Circ bad_food = new Circ();

        public Form1()
        {
            InitializeComponent();
            // Дефолтные настройки
            new Settings();

            // Задаем по таймеру скорость игры
            gameTimer.Interval = 1000 / Settings.Speed;
            gameTimer.Tick += UpdateScreen;
            gameTimer.Start();

            // Начинаем новую игру
            StartGame();
           
        }

        public void StartGame()
        {
            laGameOver.Visible = false;

            // Дефолтные настройки
            new Settings();

            // Создаем объект змейки
            Snake.Clear();

            Circ head = new Circ();
            head.X = 10;
            head.Y = 5;
            Snake.Add(head);


            laScore.Text = Settings.Score.ToString();
            GenerateFood();
            Generate_Bad_Food();
            

        }

        // Методом рандома ставим еду
        private void GenerateFood()
        {
            int maxXPos = pictureBox1.Size.Width / Settings.Width;
            int maxYPos = pictureBox1.Size.Height / Settings.Height;

            Random random = new Random();
            food = new Circ();
            food.X = random.Next(0, maxXPos);
            food.Y = random.Next(0, maxYPos);
        }

        // Методом рандома генерируем плохую еду
        private void Generate_Bad_Food()
        {
            int maxXPos = pictureBox1.Size.Width / Settings.Width;
            int maxYPos = pictureBox1.Size.Height / Settings.Height;

            Random random = new Random();
            bad_food = new Circ();
            bad_food.X = random.Next(0, maxXPos);
            bad_food.Y = random.Next(0, maxYPos);
            timer_bad_food.Enabled = true;
        }


        private void UpdateScreen(object sender, EventArgs e)
        {
            // Обработчик конца игры
            if (Settings.GameOver)
            {
                //Проверка на нажате кнопки Enter после конца игры
                if (Keys_input.KeyPressed(Keys.Enter))
                {
                    StartGame();
                }

                // Возвращение в главное меню
                if (Keys_input.KeyPressed(Keys.Escape))
                {
                    Application.Restart();
                }

            }
            else
            {
                if (Keys_input.KeyPressed(Keys.Right) && Settings.direction != Direction.Left)
                    Settings.direction = Direction.Right;
                else if (Keys_input.KeyPressed(Keys.Left) && Settings.direction != Direction.Right)
                    Settings.direction = Direction.Left;
                else if (Keys_input.KeyPressed(Keys.Up) && Settings.direction != Direction.Down)
                    Settings.direction = Direction.Up;
                else if (Keys_input.KeyPressed(Keys.Down) && Settings.direction != Direction.Up)
                    Settings.direction = Direction.Down;

                MovePlayer();
            }

            pictureBox1.Invalidate();

        }



        private void MovePlayer() // Движение змейкой
        {
            for (int i = Snake.Count - 1; i >= 0; i--)
            {
                // Создаем змейку
                if (i == 0)
                {
                    switch (Settings.direction)
                    {
                        case Direction.Right:
                            Snake[i].X++;
                            break;
                        case Direction.Left:
                            Snake[i].X--;
                            break;
                        case Direction.Up:
                            Snake[i].Y--;
                            break;
                        case Direction.Down:
                            Snake[i].Y++;
                            break;
                    }



                    int maxXPos = pictureBox1.Size.Width / Settings.Width;
                    int maxYPos = pictureBox1.Size.Height / Settings.Height;

                    // Коллизии с границей формы
                    if (Snake[i].X < 0 || Snake[i].Y < 0
                        || Snake[i].X >= maxXPos || Snake[i].Y >= maxYPos)
                    {
                        Die();
                    }


                    //Коллизии с телом змейки
                    for (int j = 1; j < Snake.Count; j++)
                    {
                        if (Snake[i].X == Snake[j].X &&
                           Snake[i].Y == Snake[j].Y)
                        {
                            Die();
                        }
                    }

                    //Коллизии с едой
                    if (Snake[0].X == food.X && Snake[0].Y == food.Y)
                    {
                        Eat();
                    }

                    //Коллизии с плохой едою
                    if (Snake[0].X == bad_food.X && Snake[0].Y == bad_food.Y)
                    {
                        Die();
                    }

                }
                else
                {
                    //Двигаем тело
                    Snake[i].X = Snake[i - 1].X;
                    Snake[i].Y = Snake[i - 1].Y;
                }
            }
        }

        
        private void Eat()
        {
            //Добавление новых объектов к змейке
            Circ circle = new Circ
            {
                X = Snake[Snake.Count - 1].X,
                Y = Snake[Snake.Count - 1].Y
            };
            Snake.Add(circle);

            //Увеличиваем очки
            Settings.Score += Settings.Points;
            laScore.Text = Settings.Score.ToString();

            GenerateFood();
            Generate_Bad_Food();
        }

        private void Die() // Умираем
        {
            Settings.GameOver = true;
        }


        

       

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            Keys_input.ChangeState(e.KeyCode, false);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            Keys_input.ChangeState(e.KeyCode, true);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics XResult = e.Graphics;

            if (!Settings.GameOver)
            {
                // задаем цвет
                Brush snakeColour;

                // Рисуем змейку
                for (int i = 0; i < Snake.Count; i++)
                {

                    if (i == 0)
                        snakeColour = Brushes.Black;     //Рисуем голову
                    else
                        snakeColour = Brushes.Black;    //Остальное тело,когда змейка подбирает еду

                    //Рисуем змейку
                    XResult.FillEllipse(snakeColour,
                        new Rectangle(Snake[i].X * Settings.Width,
                                      Snake[i].Y * Settings.Height,
                                      Settings.Width, Settings.Height));


                    //Рисуем еду для змейки
                    XResult.FillEllipse(Brushes.Red,
                        new Rectangle(food.X * Settings.Width,
                             food.Y * Settings.Height, Settings.Width, Settings.Height));

                    //Рисуем плохую еду для змейки
                    XResult.FillEllipse(Brushes.Yellow,
                        new Rectangle(bad_food.X * Settings.Width,
                             bad_food.Y * Settings.Height, Settings.Width, Settings.Height));

                }
            }
            else // обработчик проигрыша
            {
                string gameOver = "Игра окончена !!! \nВаши очки: " + Settings.Score + "\nНажмите Enter для новой игры";
                laGameOver.Text = gameOver;
                laGameOver.Visible = true;
            }
        }

        private void timer_bad_food_Tick(object sender, EventArgs e) //задаем по таймеру генерацию плохой еды
        {
            int maxXPos = pictureBox1.Size.Width / Settings.Width;
            int maxYPos = pictureBox1.Size.Height / Settings.Height;
            bad_food = new Circ();
            Random random = new Random();
            bad_food.X = random.Next(5, maxXPos);
            bad_food.Y = random.Next(5, maxYPos);

        }
    }


}
