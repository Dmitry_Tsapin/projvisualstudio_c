﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake_Game
{
    class Circ
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Circ ()
        {
            X = 0;
            Y = 0;
        }
    }
}
