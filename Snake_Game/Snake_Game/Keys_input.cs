﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Windows.Forms;

namespace Snake_Game
{
    internal class Keys_input
    {
        // Загружаем список кнопок
        private static Hashtable keyTable = new Hashtable();

        // Обработчик нажатия на кнопку клаватуры 
        public static bool KeyPressed(Keys key)
        {
            if (keyTable[key] == null)
            {
                return false;
            }

            return (bool)keyTable[key];
        }

        // Определяем,какая кнопка на клавиатуре нажата
        public static void ChangeState(Keys key, bool state)
        {
            keyTable[key] = state;
        }
    }
}
