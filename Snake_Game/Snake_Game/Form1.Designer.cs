﻿namespace Snake_Game
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gameTimer = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.laScore = new System.Windows.Forms.Label();
            this.laGameOver = new System.Windows.Forms.Label();
            this.timer_bad_food = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // gameTimer
            // 
            this.gameTimer.Enabled = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Lime;
            this.pictureBox1.Location = new System.Drawing.Point(-1, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(511, 444);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // laScore
            // 
            this.laScore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.laScore.AutoSize = true;
            this.laScore.BackColor = System.Drawing.Color.White;
            this.laScore.Font = new System.Drawing.Font("Showcard Gothic", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.laScore.Location = new System.Drawing.Point(516, 184);
            this.laScore.Name = "laScore";
            this.laScore.Size = new System.Drawing.Size(114, 42);
            this.laScore.TabIndex = 1;
            this.laScore.Text = "Очки:";
            // 
            // laGameOver
            // 
            this.laGameOver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.laGameOver.AutoSize = true;
            this.laGameOver.BackColor = System.Drawing.Color.Lime;
            this.laGameOver.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laGameOver.Location = new System.Drawing.Point(-6, 242);
            this.laGameOver.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.laGameOver.MinimumSize = new System.Drawing.Size(81, 29);
            this.laGameOver.Name = "laGameOver";
            this.laGameOver.Size = new System.Drawing.Size(81, 29);
            this.laGameOver.TabIndex = 2;
            this.laGameOver.Text = "label2";
            // 
            // timer_bad_food
            // 
            this.timer_bad_food.Interval = 800;
            this.timer_bad_food.Tick += new System.EventHandler(this.timer_bad_food_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 450);
            this.Controls.Add(this.laGameOver);
            this.Controls.Add(this.laScore);
            this.Controls.Add(this.pictureBox1);
            this.MinimumSize = new System.Drawing.Size(682, 497);
            this.Name = "Form1";
            this.Text = "Змейка";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Timer gameTimer;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label laScore;
        private System.Windows.Forms.Label laGameOver;
        private System.Windows.Forms.Timer timer_bad_food;
    }
}

