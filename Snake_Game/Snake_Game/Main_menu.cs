﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace Snake_Game
{
    public partial class Main_menu : Form
    {
        public Main_menu()
        {
            InitializeComponent();

        }
        Form1 f = new Form1();

        private void bu_start_Click(object sender, EventArgs e)
        {
            new Thread(() => { Application.Run(new Form1()); }).Start();
            this.Close();
            
        }
    }
}
