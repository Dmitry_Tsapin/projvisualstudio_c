﻿namespace Snake_Game
{
    partial class Main_menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_menu));
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bu_start = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.easy = new System.Windows.Forms.CheckBox();
            this.medium = new System.Windows.Forms.CheckBox();
            this.hard = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Verdana", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(155, -1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(495, 81);
            this.label1.TabIndex = 0;
            this.label1.Text = "Добро пожаловать в игру \"Змейка\"!";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(71, 83);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(661, 199);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // bu_start
            // 
            this.bu_start.Location = new System.Drawing.Point(314, 410);
            this.bu_start.Name = "bu_start";
            this.bu_start.Size = new System.Drawing.Size(163, 38);
            this.bu_start.TabIndex = 2;
            this.bu_start.Text = "Начать игру";
            this.bu_start.UseVisualStyleBackColor = true;
            this.bu_start.Click += new System.EventHandler(this.bu_start_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(173, 285);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(461, 34);
            this.label2.TabIndex = 3;
            this.label2.Text = "Выберите уровень сложности:";
            // 
            // easy
            // 
            this.easy.AutoSize = true;
            this.easy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.easy.Location = new System.Drawing.Point(179, 334);
            this.easy.Name = "easy";
            this.easy.Size = new System.Drawing.Size(89, 29);
            this.easy.TabIndex = 4;
            this.easy.Text = "Легко";
            this.easy.UseVisualStyleBackColor = true;
            // 
            // medium
            // 
            this.medium.AutoSize = true;
            this.medium.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.medium.Location = new System.Drawing.Point(369, 334);
            this.medium.Name = "medium";
            this.medium.Size = new System.Drawing.Size(103, 29);
            this.medium.TabIndex = 5;
            this.medium.Text = "Средне";
            this.medium.UseVisualStyleBackColor = true;
            // 
            // hard
            // 
            this.hard.AutoSize = true;
            this.hard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hard.Location = new System.Drawing.Point(589, 334);
            this.hard.Name = "hard";
            this.hard.Size = new System.Drawing.Size(109, 29);
            this.hard.TabIndex = 6;
            this.hard.Text = "Сложно";
            this.hard.UseVisualStyleBackColor = true;
            // 
            // Main_menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lime;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.hard);
            this.Controls.Add(this.medium);
            this.Controls.Add(this.easy);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bu_start);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Name = "Main_menu";
            this.Text = "Main_menu";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button bu_start;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox easy;
        private System.Windows.Forms.CheckBox medium;
        private System.Windows.Forms.CheckBox hard;
    }
}