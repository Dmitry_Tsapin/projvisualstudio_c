﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_WebBrowser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //
            buGo.Click += delegate
            {
                webBrowser1.Navigate(edURL.Text);
            };

            buBack.Click += delegate
            {
                webBrowser1.GoBack();
            };

            buForward.Click += delegate
            {
                webBrowser1.GoForward();
            };

            buReload.Click += delegate
            {
                webBrowser1.Refresh();
            };

            buStop.Click += delegate
            {
                webBrowser1.Stop();
            };

            webBrowser1.DocumentCompleted += delegate
            {
                edURL.Text = webBrowser1.Url.ToString();
                tbLog.Text += "\r\n" + edURL.Text;

            };
            edURL.KeyDown += EdURL_KeyDown;

        }

        private void EdURL_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                webBrowser1.Navigate(edURL.Text);
        }
    }
}
