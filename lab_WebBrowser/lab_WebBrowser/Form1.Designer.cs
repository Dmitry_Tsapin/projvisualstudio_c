﻿namespace lab_WebBrowser
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.edURL = new System.Windows.Forms.TextBox();
            this.buGo = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.buBack = new System.Windows.Forms.Button();
            this.buForward = new System.Windows.Forms.Button();
            this.buReload = new System.Windows.Forms.Button();
            this.buStop = new System.Windows.Forms.Button();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // edURL
            // 
            this.edURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edURL.Location = new System.Drawing.Point(13, 13);
            this.edURL.Name = "edURL";
            this.edURL.Size = new System.Drawing.Size(690, 22);
            this.edURL.TabIndex = 0;
            this.edURL.Text = "https://yandex.ru/\r\n";
            // 
            // buGo
            // 
            this.buGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buGo.BackColor = System.Drawing.Color.Lime;
            this.buGo.Location = new System.Drawing.Point(723, 12);
            this.buGo.Name = "buGo";
            this.buGo.Size = new System.Drawing.Size(75, 23);
            this.buGo.TabIndex = 1;
            this.buGo.Text = "Go";
            this.buGo.UseVisualStyleBackColor = false;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser1.Location = new System.Drawing.Point(3, 41);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(515, 356);
            this.webBrowser1.TabIndex = 2;
            // 
            // buBack
            // 
            this.buBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buBack.Location = new System.Drawing.Point(35, 403);
            this.buBack.Name = "buBack";
            this.buBack.Size = new System.Drawing.Size(106, 35);
            this.buBack.TabIndex = 3;
            this.buBack.Text = "<< Back";
            this.buBack.UseVisualStyleBackColor = false;
            // 
            // buForward
            // 
            this.buForward.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buForward.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.buForward.Location = new System.Drawing.Point(268, 402);
            this.buForward.Name = "buForward";
            this.buForward.Size = new System.Drawing.Size(91, 36);
            this.buForward.TabIndex = 4;
            this.buForward.Text = "Forward >>";
            this.buForward.UseVisualStyleBackColor = false;
            // 
            // buReload
            // 
            this.buReload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buReload.BackColor = System.Drawing.Color.Lime;
            this.buReload.Location = new System.Drawing.Point(491, 403);
            this.buReload.Name = "buReload";
            this.buReload.Size = new System.Drawing.Size(75, 35);
            this.buReload.TabIndex = 5;
            this.buReload.Text = "Reload";
            this.buReload.UseVisualStyleBackColor = false;
            // 
            // buStop
            // 
            this.buStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buStop.BackColor = System.Drawing.Color.Red;
            this.buStop.Location = new System.Drawing.Point(680, 402);
            this.buStop.Name = "buStop";
            this.buStop.Size = new System.Drawing.Size(75, 36);
            this.buStop.TabIndex = 6;
            this.buStop.Text = "Stop";
            this.buStop.UseVisualStyleBackColor = false;
            // 
            // tbLog
            // 
            this.tbLog.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbLog.Location = new System.Drawing.Point(524, 41);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbLog.Size = new System.Drawing.Size(274, 355);
            this.tbLog.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Yellow;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbLog);
            this.Controls.Add(this.buStop);
            this.Controls.Add(this.buReload);
            this.Controls.Add(this.buForward);
            this.Controls.Add(this.buBack);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.buGo);
            this.Controls.Add(this.edURL);
            this.MinimumSize = new System.Drawing.Size(818, 497);
            this.Name = "Form1";
            this.Text = "Web browser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox edURL;
        private System.Windows.Forms.Button buGo;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Button buBack;
        private System.Windows.Forms.Button buForward;
        private System.Windows.Forms.Button buReload;
        private System.Windows.Forms.Button buStop;
        private System.Windows.Forms.TextBox tbLog;
    }
}

