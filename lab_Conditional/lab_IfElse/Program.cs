﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_IfElse
{
    class Program
    {
        static void Main(string[] args)
        {
            AAA(10);
            AAA(30);
        }
        private static void AAA (int b)
        {
            if (b < 10)
            {
                Console.WriteLine("{0} меньше 10", b);
            }
            else if (b < 20)
            {
                Console.WriteLine("{0} меньше 20", b);
            }
            else 
            {
                Console.WriteLine("{0} больше 20", b);
            }
        }
    }
}
