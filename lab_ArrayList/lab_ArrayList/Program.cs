﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_ArrayList
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList list = new ArrayList();
            list.Add(3.14);
            list.Add(256);
            list.Add(256 * 2);
            list.Add(256 * 3);
            list.Add(256 * 4);
            list.Add(256 * 5);
            list.Add(256 * 6);
            list.AddRange(new string[] { "Hello", "world" });
            //
            foreach(object o in list)
            {
                Console.WriteLine(o);
            }
            Console.WriteLine();
            //
            list.RemoveAt(1);
            list.RemoveAt(2);
            list.RemoveAt(0); // удаляем первый элемент
            list.Add(256 * 7);
            //list.Reverse(); // переворачиваем список
            //Console.WriteLine(list[0]); //получение элемента по индексу
            //
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }

            Console.ReadLine();

        }
    }
}
