﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_Calc
{
    public partial class Fm1 : Form
    {
        public Fm1()
        {
            InitializeComponent();
        }

        private void buAdd_Click(object sender, EventArgs e)
        {
            //laResult.Text = textBox1.Text + textBox2.Text; пример со сложением строк
            double xResult = Convert.ToDouble (textBox1.Text) + Convert.ToDouble (textBox2.Text);
            laResult.Text = xResult.ToString();
            tbLog.Text += String.Format("{0} + {1} = {2}" + "\r\n", textBox1.Text, textBox2.Text, laResult.Text);
            
        }

        private void butMinus_Click(object sender, EventArgs e)
        {
            double xResult = Convert.ToDouble(textBox1.Text) - Convert.ToDouble(textBox2.Text);
            laResult.Text = xResult.ToString();
            tbLog.Text += String.Format("{0} - {1} = {2}" + "\r\n", textBox1.Text, textBox2.Text, laResult.Text);


        }

        private void butMulti_Click(object sender, EventArgs e)
        {

            double xResult = Convert.ToDouble(textBox1.Text) * Convert.ToDouble(textBox2.Text);
            laResult.Text = xResult.ToString();
            tbLog.Text += String.Format("{0} * {1} = {2}" + "\r\n", textBox1.Text, textBox2.Text, laResult.Text);

        }

        private void butDivision_Click(object sender, EventArgs e)
        {
            double xResult = Convert.ToDouble(textBox1.Text) / Convert.ToDouble(textBox2.Text);
            laResult.Text = xResult.ToString();
            tbLog.Text += String.Format("{0} / {1} = {2}" + "\r\n", textBox1.Text, textBox2.Text, laResult.Text);

        }

        private void buClear_Click(object sender, EventArgs e)
        {
            tbLog.Text = "";
        }
    }
}
