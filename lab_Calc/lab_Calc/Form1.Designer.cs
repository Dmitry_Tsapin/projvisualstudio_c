﻿namespace lab_Calc
{
    partial class Fm1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.laResult = new System.Windows.Forms.Label();
            this.buAdd = new System.Windows.Forms.Button();
            this.butMinus = new System.Windows.Forms.Button();
            this.butMulti = new System.Windows.Forms.Button();
            this.butDivision = new System.Windows.Forms.Button();
            this.tbLog = new System.Windows.Forms.TextBox();
            this.buClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(366, 36);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 45);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "0";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox2.Location = new System.Drawing.Point(366, 87);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 45);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = "0";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(284, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 39);
            this.label1.TabIndex = 2;
            this.label1.Text = "A =";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(284, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 39);
            this.label2.TabIndex = 3;
            this.label2.Text = "B =";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(224, 354);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(190, 39);
            this.label3.TabIndex = 4;
            this.label3.Text = "Результат:";
            // 
            // laResult
            // 
            this.laResult.AutoSize = true;
            this.laResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laResult.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.laResult.Location = new System.Drawing.Point(420, 354);
            this.laResult.Name = "laResult";
            this.laResult.Size = new System.Drawing.Size(134, 39);
            this.laResult.TabIndex = 5;
            this.laResult.Text = "(Ответ)";
            // 
            // buAdd
            // 
            this.buAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buAdd.Location = new System.Drawing.Point(178, 163);
            this.buAdd.Name = "buAdd";
            this.buAdd.Size = new System.Drawing.Size(75, 43);
            this.buAdd.TabIndex = 6;
            this.buAdd.Text = "+";
            this.buAdd.UseVisualStyleBackColor = true;
            this.buAdd.Click += new System.EventHandler(this.buAdd_Click);
            // 
            // butMinus
            // 
            this.butMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butMinus.Location = new System.Drawing.Point(291, 163);
            this.butMinus.Name = "butMinus";
            this.butMinus.Size = new System.Drawing.Size(75, 43);
            this.butMinus.TabIndex = 7;
            this.butMinus.Text = "-";
            this.butMinus.UseVisualStyleBackColor = true;
            this.butMinus.Click += new System.EventHandler(this.butMinus_Click);
            // 
            // butMulti
            // 
            this.butMulti.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butMulti.Location = new System.Drawing.Point(404, 163);
            this.butMulti.Name = "butMulti";
            this.butMulti.Size = new System.Drawing.Size(75, 43);
            this.butMulti.TabIndex = 8;
            this.butMulti.Text = "*";
            this.butMulti.UseVisualStyleBackColor = true;
            this.butMulti.Click += new System.EventHandler(this.butMulti_Click);
            // 
            // butDivision
            // 
            this.butDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butDivision.Location = new System.Drawing.Point(527, 163);
            this.butDivision.Name = "butDivision";
            this.butDivision.Size = new System.Drawing.Size(75, 43);
            this.butDivision.TabIndex = 9;
            this.butDivision.Text = ":";
            this.butDivision.UseVisualStyleBackColor = true;
            this.butDivision.Click += new System.EventHandler(this.butDivision_Click);
            // 
            // tbLog
            // 
            this.tbLog.Location = new System.Drawing.Point(632, -1);
            this.tbLog.Multiline = true;
            this.tbLog.Name = "tbLog";
            this.tbLog.Size = new System.Drawing.Size(170, 458);
            this.tbLog.TabIndex = 10;
            // 
            // buClear
            // 
            this.buClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buClear.Location = new System.Drawing.Point(231, 274);
            this.buClear.Name = "buClear";
            this.buClear.Size = new System.Drawing.Size(293, 66);
            this.buClear.TabIndex = 11;
            this.buClear.Text = "Очистить лог";
            this.buClear.UseVisualStyleBackColor = true;
            this.buClear.Click += new System.EventHandler(this.buClear_Click);
            // 
            // Fm1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Red;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buClear);
            this.Controls.Add(this.tbLog);
            this.Controls.Add(this.butDivision);
            this.Controls.Add(this.butMulti);
            this.Controls.Add(this.butMinus);
            this.Controls.Add(this.buAdd);
            this.Controls.Add(this.laResult);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.MinimumSize = new System.Drawing.Size(818, 497);
            this.Name = "Fm1";
            this.Text = "Калькулятор";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label laResult;
        private System.Windows.Forms.Button buAdd;
        private System.Windows.Forms.Button butMinus;
        private System.Windows.Forms.Button butMulti;
        private System.Windows.Forms.Button butDivision;
        private System.Windows.Forms.TextBox tbLog;
        private System.Windows.Forms.Button buClear;
    }
}

