﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_Test_Simple
{
    public partial class Form1 : Form
    {
        private Test t;
        public Form1()
        {
            InitializeComponent();
            //
            buStart.Click += BuStart_Click;
            t = new Test();
            t.DoReset();
            t.Change += Event_Change;

        }

        private void Event_Change(object sender, EventArgs e)
        {
            label5.Text = String.Format("Верно = {0} ", t.CountIncorrect.ToString());
            label6.Text = String.Format("Неверно = {0} ", t.CountCorrect.ToString());
            if (t.CountIncorrect > t.CountCorrect)
            {
                label10.Show();
                label11.Hide();
            }
            else
            {
                label10.Hide();
                label11.Show();
            }

        }

        private void BuStart_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = tabControl1.SelectedIndex + 1;
            timer1.Enabled = true;
            progressBar1.Value = 25;
        }

        private void BuAll_Click(object sender, EventArgs e)
        {
            Button b = (sender as Button);
            if (b.Tag != null)
            {   if (tabControl1.SelectedIndex == 1)
                {
                    label7.Text = String.Format("1-й вопрос-Верно");
                    progressBar1.Value = 50;
                }
                if (tabControl1.SelectedIndex == 2)
                {
                    label8.Text = String.Format("2-й вопрос-Верно");
                    progressBar1.Value = 75;
                }
                if (tabControl1.SelectedIndex == 3)
                {
                    label9.Text = String.Format("3-й вопрос-Верно");
                    progressBar1.Value = 100;
                }
                t.DoAnswer(true);
                
                // верно
            }
            else
            {
                if (tabControl1.SelectedIndex == 1)
                {
                    
                    label7.Text = String.Format("1-й вопрос-Неверно");
                    progressBar1.Value = 50;
                }
                if (tabControl1.SelectedIndex == 2)
                {
                    label8.Text = String.Format("2-й вопрос-Неверно");
                    progressBar1.Value = 75;
                }
                if (tabControl1.SelectedIndex == 3)
                {
                    label9.Text = String.Format("3-й вопрос-Неверно");
                    progressBar1.Value = 100;
                }
                t.DoAnswer(false);
                
                // неверно
            }
            tabControl1.SelectedIndex = tabControl1.SelectedIndex + 1;
        }

        private void button13_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = tabControl1.SelectedIndex = 0;
            t.DoReset();
            progressBar1.Value = 0;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
           
            if (progressBar1.Value == 100)
            {
                timer1.Enabled = false;
            }
        }
    }
}
