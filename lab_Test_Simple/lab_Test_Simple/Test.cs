﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Test_Simple
{
    class Test
    {
        public int CountCorrect { get; protected set; }
        public int CountIncorrect { get; protected set; }
        public bool AnswerCorrect { get; protected set; }
        public event EventHandler Change;
        public void DoReset()
        {
            CountCorrect = 0;
            CountIncorrect = 0;
            DoContinue();
        }

        public void DoContinue()
        {
            if (Change != null)
                Change(this, EventArgs.Empty);
        }
        public void DoAnswer (bool v)
        {
            if (v == AnswerCorrect)
            {
                CountCorrect++;
                v = true;
            }

            else
            {
                CountIncorrect++;
                v = false;
            }
            DoContinue();
        }
    }
}
