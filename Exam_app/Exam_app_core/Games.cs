﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exam_app_core
{
    public class Games
    {
        public int CountCorrect { get; protected set; }
        public int CountWrong { get; protected set; }
        public int Level;
        public int Points { get; protected set; }
        public bool Code { get; protected set; }
        public string CodeText { get; protected set; }
        public static int time = 60;
        public event EventHandler GoScreenAnswer;
        public event EventHandler Change;

        public void DoContinue()
        {
           if (Change != null)
                Change(this, EventArgs.Empty);


        }

        public void DoReset()
        {
            CountCorrect = 0;
            CountWrong = 0;
            DoContinue();
        }

        

        public void DoAnswer(bool v)
        {
            if (v == Code)
            {
                Points--;
                CountCorrect++;
                
                
            }
            else if (Points < 0)
            {
                Application.Restart();

            }
            else if (Level < 0)
            {
                Application.Restart();

            }
            else
            {
                Points++;
                CountWrong++;
                

            }
             
            DoContinue();
             
        }
    }
}
