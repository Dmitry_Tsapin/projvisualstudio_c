﻿namespace Exam_app
{
    partial class Game_exit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.laLevel = new System.Windows.Forms.Label();
            this.bu_Res = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // laLevel
            // 
            this.laLevel.AutoSize = true;
            this.laLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laLevel.Location = new System.Drawing.Point(247, 114);
            this.laLevel.MaximumSize = new System.Drawing.Size(238, 32);
            this.laLevel.Name = "laLevel";
            this.laLevel.Size = new System.Drawing.Size(238, 32);
            this.laLevel.TabIndex = 11;
            this.laLevel.Text = "Спасибо за игру!";
            // 
            // bu_Res
            // 
            this.bu_Res.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bu_Res.Location = new System.Drawing.Point(253, 247);
            this.bu_Res.Name = "bu_Res";
            this.bu_Res.Size = new System.Drawing.Size(212, 69);
            this.bu_Res.TabIndex = 13;
            this.bu_Res.Text = "Выйти в меню";
            this.bu_Res.UseVisualStyleBackColor = true;
            this.bu_Res.Click += new System.EventHandler(this.bu_Res_Click);
            // 
            // Game_exit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.bu_Res);
            this.Controls.Add(this.laLevel);
            this.MaximumSize = new System.Drawing.Size(818, 497);
            this.MinimumSize = new System.Drawing.Size(818, 497);
            this.Name = "Game_exit";
            this.Text = "Game_exit";
            this.Load += new System.EventHandler(this.Game_exit_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label laLevel;
        private System.Windows.Forms.Button bu_Res;
    }
}