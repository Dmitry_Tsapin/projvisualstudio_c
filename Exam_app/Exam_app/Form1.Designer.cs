﻿namespace Exam_app
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.bu_Start = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.bu_Contacts = new System.Windows.Forms.Button();
            this.bu_Settings = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.laLevel = new System.Windows.Forms.Label();
            this.laIncorrect = new System.Windows.Forms.Label();
            this.bu_Restart = new System.Windows.Forms.Button();
            this.bu_Answer = new System.Windows.Forms.Button();
            this.bu_Gen = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.laPoints = new System.Windows.Forms.Label();
            this.laCorrect = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bu_Exit = new System.Windows.Forms.Button();
            this.bu_About = new System.Windows.Forms.Button();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(224, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(313, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Добро пожаловать в игру!";
            // 
            // bu_Start
            // 
            this.bu_Start.BackColor = System.Drawing.SystemColors.Control;
            this.bu_Start.Location = new System.Drawing.Point(267, 312);
            this.bu_Start.Name = "bu_Start";
            this.bu_Start.Size = new System.Drawing.Size(235, 54);
            this.bu_Start.TabIndex = 1;
            this.bu_Start.Text = "Начать игру";
            this.bu_Start.UseVisualStyleBackColor = false;
            this.bu_Start.Click += new System.EventHandler(this.bu_Start_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.ItemSize = new System.Drawing.Size(0, 1);
            this.tabControl1.Location = new System.Drawing.Point(1, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(798, 448);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tabPage1.Controls.Add(this.bu_About);
            this.tabPage1.Controls.Add(this.bu_Contacts);
            this.tabPage1.Controls.Add(this.bu_Settings);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.bu_Start);
            this.tabPage1.Location = new System.Drawing.Point(4, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(790, 439);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // bu_Contacts
            // 
            this.bu_Contacts.Location = new System.Drawing.Point(267, 163);
            this.bu_Contacts.Name = "bu_Contacts";
            this.bu_Contacts.Size = new System.Drawing.Size(235, 54);
            this.bu_Contacts.TabIndex = 3;
            this.bu_Contacts.Text = "Контакты";
            this.bu_Contacts.UseVisualStyleBackColor = true;
            this.bu_Contacts.Click += new System.EventHandler(this.bu_Contacts_Click);
            // 
            // bu_Settings
            // 
            this.bu_Settings.Location = new System.Drawing.Point(267, 235);
            this.bu_Settings.Name = "bu_Settings";
            this.bu_Settings.Size = new System.Drawing.Size(235, 54);
            this.bu_Settings.TabIndex = 2;
            this.bu_Settings.Text = "Настройки";
            this.bu_Settings.UseVisualStyleBackColor = true;
            this.bu_Settings.Click += new System.EventHandler(this.bu_Settings_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tabPage2.Controls.Add(this.laLevel);
            this.tabPage2.Controls.Add(this.laIncorrect);
            this.tabPage2.Controls.Add(this.bu_Restart);
            this.tabPage2.Controls.Add(this.bu_Answer);
            this.tabPage2.Controls.Add(this.bu_Gen);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.tableLayoutPanel1);
            this.tabPage2.Controls.Add(this.laPoints);
            this.tabPage2.Controls.Add(this.laCorrect);
            this.tabPage2.Location = new System.Drawing.Point(4, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(790, 439);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            // 
            // laLevel
            // 
            this.laLevel.AutoSize = true;
            this.laLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laLevel.Location = new System.Drawing.Point(587, 182);
            this.laLevel.Name = "laLevel";
            this.laLevel.Size = new System.Drawing.Size(135, 32);
            this.laLevel.TabIndex = 10;
            this.laLevel.Text = "Уровень:";
            // 
            // laIncorrect
            // 
            this.laIncorrect.AutoSize = true;
            this.laIncorrect.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laIncorrect.Location = new System.Drawing.Point(295, 25);
            this.laIncorrect.Name = "laIncorrect";
            this.laIncorrect.Size = new System.Drawing.Size(138, 32);
            this.laIncorrect.TabIndex = 9;
            this.laIncorrect.Text = "Неверно:";
            // 
            // bu_Restart
            // 
            this.bu_Restart.Location = new System.Drawing.Point(7, 385);
            this.bu_Restart.Name = "bu_Restart";
            this.bu_Restart.Size = new System.Drawing.Size(131, 47);
            this.bu_Restart.TabIndex = 8;
            this.bu_Restart.Text = "Перезапустить игру";
            this.bu_Restart.UseVisualStyleBackColor = true;
            this.bu_Restart.Click += new System.EventHandler(this.bu_Restart_Click);
            // 
            // bu_Answer
            // 
            this.bu_Answer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bu_Answer.Location = new System.Drawing.Point(624, 349);
            this.bu_Answer.MaximumSize = new System.Drawing.Size(144, 82);
            this.bu_Answer.MinimumSize = new System.Drawing.Size(144, 82);
            this.bu_Answer.Name = "bu_Answer";
            this.bu_Answer.Size = new System.Drawing.Size(144, 82);
            this.bu_Answer.TabIndex = 7;
            this.bu_Answer.Text = "Выйти из игры";
            this.bu_Answer.UseVisualStyleBackColor = true;
            this.bu_Answer.Click += new System.EventHandler(this.bu_Answer_Click);
            // 
            // bu_Gen
            // 
            this.bu_Gen.Location = new System.Drawing.Point(0, 210);
            this.bu_Gen.Name = "bu_Gen";
            this.bu_Gen.Size = new System.Drawing.Size(141, 72);
            this.bu_Gen.TabIndex = 6;
            this.bu_Gen.Text = "Сгенерировать поле";
            this.bu_Gen.UseVisualStyleBackColor = true;
            this.bu_Gen.Click += new System.EventHandler(this.bu_Gen_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(24, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(733, 32);
            this.label9.TabIndex = 5;
            this.label9.Text = "Найдите все синие прямоугольники (кликайте по ним)";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.button12, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.button11, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.button10, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.button9, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.button8, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.button7, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.button6, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.button5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button4, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.button3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(152, 129);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(429, 239);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(217, 198);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(208, 33);
            this.button12.TabIndex = 11;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.Bu_All);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(3, 198);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(208, 33);
            this.button11.TabIndex = 10;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.Bu_All);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(217, 159);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(208, 33);
            this.button10.TabIndex = 9;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.Bu_All);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(3, 159);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(208, 33);
            this.button9.TabIndex = 8;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Bu_All);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(217, 120);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(208, 33);
            this.button8.TabIndex = 7;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Bu_All);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(3, 120);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(208, 33);
            this.button7.TabIndex = 6;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Bu_All);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(217, 81);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(208, 33);
            this.button6.TabIndex = 5;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Bu_All);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(3, 81);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(208, 33);
            this.button5.TabIndex = 4;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Bu_All);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(217, 42);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(208, 33);
            this.button4.TabIndex = 3;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Bu_All);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(3, 42);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(208, 33);
            this.button3.TabIndex = 2;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Bu_All);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(217, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(208, 33);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Bu_All);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(208, 33);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Bu_All);
            // 
            // laPoints
            // 
            this.laPoints.AutoSize = true;
            this.laPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laPoints.Location = new System.Drawing.Point(618, 25);
            this.laPoints.Name = "laPoints";
            this.laPoints.Size = new System.Drawing.Size(90, 32);
            this.laPoints.TabIndex = 2;
            this.laPoints.Text = "Очки:";
            // 
            // laCorrect
            // 
            this.laCorrect.AutoSize = true;
            this.laCorrect.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.laCorrect.Location = new System.Drawing.Point(24, 25);
            this.laCorrect.Name = "laCorrect";
            this.laCorrect.Size = new System.Drawing.Size(106, 32);
            this.laCorrect.TabIndex = 1;
            this.laCorrect.Text = "Верно:";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tabPage3.Controls.Add(this.tableLayoutPanel2);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Location = new System.Drawing.Point(4, 5);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(790, 439);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.button13, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.button14, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.button15, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.button16, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.button17, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.button18, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.button19, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.button20, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.button21, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.button22, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.button23, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.button24, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(159, 113);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(429, 239);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(217, 198);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(208, 33);
            this.button13.TabIndex = 11;
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(3, 198);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(208, 33);
            this.button14.TabIndex = 10;
            this.button14.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(217, 159);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(208, 33);
            this.button15.TabIndex = 9;
            this.button15.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(3, 159);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(208, 33);
            this.button16.TabIndex = 8;
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(217, 120);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(208, 33);
            this.button17.TabIndex = 7;
            this.button17.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(3, 120);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(208, 33);
            this.button18.TabIndex = 6;
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(217, 81);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(208, 33);
            this.button19.TabIndex = 5;
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(3, 81);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(208, 33);
            this.button20.TabIndex = 4;
            this.button20.UseVisualStyleBackColor = true;
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(217, 42);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(208, 33);
            this.button21.TabIndex = 3;
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(3, 42);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(208, 33);
            this.button22.TabIndex = 2;
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(217, 3);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(208, 33);
            this.button23.TabIndex = 1;
            this.button23.UseVisualStyleBackColor = true;
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(3, 3);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(208, 33);
            this.button24.TabIndex = 0;
            this.button24.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(636, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 32);
            this.label5.TabIndex = 3;
            this.label5.Text = "Очки:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(7, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 32);
            this.label4.TabIndex = 2;
            this.label4.Text = "Уровень:";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.tableLayoutPanel3);
            this.tabPage4.Location = new System.Drawing.Point(4, 5);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(790, 439);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(635, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 32);
            this.label8.TabIndex = 6;
            this.label8.Text = "Очки:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(20, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 32);
            this.label7.TabIndex = 5;
            this.label7.Text = "Уровень:";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.button25, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.button26, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.button27, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.button28, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.button29, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.button30, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.button31, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.button32, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.button33, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.button34, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.button36, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.button35, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(171, 105);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(429, 239);
            this.tableLayoutPanel3.TabIndex = 4;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(217, 198);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(208, 33);
            this.button25.TabIndex = 11;
            this.button25.UseVisualStyleBackColor = true;
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(3, 198);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(208, 33);
            this.button26.TabIndex = 10;
            this.button26.UseVisualStyleBackColor = true;
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(217, 159);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(208, 33);
            this.button27.TabIndex = 9;
            this.button27.UseVisualStyleBackColor = true;
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(3, 159);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(208, 33);
            this.button28.TabIndex = 8;
            this.button28.UseVisualStyleBackColor = true;
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(217, 120);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(208, 33);
            this.button29.TabIndex = 7;
            this.button29.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(3, 120);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(208, 33);
            this.button30.TabIndex = 6;
            this.button30.UseVisualStyleBackColor = true;
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(217, 81);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(208, 33);
            this.button31.TabIndex = 5;
            this.button31.UseVisualStyleBackColor = true;
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(3, 81);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(208, 33);
            this.button32.TabIndex = 4;
            this.button32.UseVisualStyleBackColor = true;
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(217, 42);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(208, 33);
            this.button33.TabIndex = 3;
            this.button33.UseVisualStyleBackColor = true;
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(3, 42);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(208, 33);
            this.button34.TabIndex = 2;
            this.button34.UseVisualStyleBackColor = true;
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(3, 3);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(208, 33);
            this.button36.TabIndex = 0;
            this.button36.UseVisualStyleBackColor = true;
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(217, 3);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(208, 33);
            this.button35.TabIndex = 1;
            this.button35.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tabPage5.Controls.Add(this.bu_Exit);
            this.tabPage5.Controls.Add(this.label2);
            this.tabPage5.Controls.Add(this.label6);
            this.tabPage5.Location = new System.Drawing.Point(4, 5);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(790, 439);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "tabPage5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(168, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(476, 29);
            this.label6.TabIndex = 0;
            this.label6.Text = "Настройки игры: Выберите режим игры";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(155, 166);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(557, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "Данный раздел пока находится в разработке :(";
            // 
            // bu_Exit
            // 
            this.bu_Exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bu_Exit.Location = new System.Drawing.Point(331, 274);
            this.bu_Exit.Name = "bu_Exit";
            this.bu_Exit.Size = new System.Drawing.Size(163, 71);
            this.bu_Exit.TabIndex = 2;
            this.bu_Exit.Text = "Вернуться в главное меню";
            this.bu_Exit.UseVisualStyleBackColor = true;
            this.bu_Exit.Click += new System.EventHandler(this.bu_Exit_Click);
            // 
            // bu_About
            // 
            this.bu_About.Location = new System.Drawing.Point(267, 87);
            this.bu_About.Name = "bu_About";
            this.bu_About.Size = new System.Drawing.Size(235, 54);
            this.bu_About.TabIndex = 4;
            this.bu_About.Text = "Как играть? (рекомендую для новичков)";
            this.bu_About.UseVisualStyleBackColor = true;
            this.bu_About.Click += new System.EventHandler(this.bu_About_Click);
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.MaximumSize = new System.Drawing.Size(818, 497);
            this.MinimumSize = new System.Drawing.Size(818, 497);
            this.Name = "Form1";
            this.Text = "Exam_app";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bu_Start;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button bu_Contacts;
        private System.Windows.Forms.Button bu_Settings;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label laPoints;
        private System.Windows.Forms.Label laCorrect;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button bu_Gen;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button bu_Answer;
        private System.Windows.Forms.Button bu_Restart;
        private System.Windows.Forms.Label laIncorrect;
        private System.Windows.Forms.Label laLevel;
        private System.Windows.Forms.Button bu_Exit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bu_About;
        private System.Windows.Forms.Timer timer;
    }
}

