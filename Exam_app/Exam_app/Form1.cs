﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Exam_app_core;
namespace Exam_app
{
    public partial class Form1 : Form
    {
        private Games g;

        public Form1()
        {
            InitializeComponent();
            g = new Games();
            g.Change += G_Change;
            g.GoScreenAnswer += G_GoScreenAnswer;

        }

        private void G_GoScreenAnswer(object sender, EventArgs e)
        {
          
        }

        private void G_Change(object sender, EventArgs e)
        {
            laCorrect.Text = String.Format("Верно = {0}", g.CountWrong.ToString());
            laIncorrect.Text = String.Format("Неверно = {0}", g.CountCorrect.ToString());
            laPoints.Text = String.Format("Очки = {0}", g.Points.ToString());
            laLevel.Text = String.Format("Уровень = {0}", g.Level.ToString());
        }

        private void bu_Start_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = tabControl1.SelectedIndex + 1;
        }

        private void bu_Settings_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = tabControl1.SelectedIndex + 4;
        }

        private void bu_Gen_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            Color[] colors = new Color[] { Color.Yellow, Color.Blue, Color.Red };
            button1.BackColor = colors[random.Next(0, colors.Length)];
            button2.BackColor = colors[random.Next(0, colors.Length)];
            button3.BackColor = colors[random.Next(0, colors.Length)];
            button4.BackColor = colors[random.Next(0, colors.Length)];
            button5.BackColor = colors[random.Next(0, colors.Length)];
            button6.BackColor = colors[random.Next(0, colors.Length)];
            button7.BackColor = colors[random.Next(0, colors.Length)];
            button8.BackColor = colors[random.Next(0, colors.Length)];
            button9.BackColor = colors[random.Next(0, colors.Length)];
            button10.BackColor = colors[random.Next(0, colors.Length)];
            button11.BackColor = colors[random.Next(0, colors.Length)];
            button12.BackColor = colors[random.Next(0, colors.Length)];
            g.Level++;
        }

        private void bu_Answer_Click(object sender, EventArgs e)
        {
            new Thread(() => { Application.Run(new Game_exit()); }).Start();
            this.Close();

        }

        private void bu_Restart_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void bu_Contacts_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Dmitry Tsapin 171-362");
        }

        private void bu_Exit_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void Bu_All(object sender, EventArgs e)
        {
            Button b = (sender as Button);
            if (b.BackColor != Color.Blue)
            {
 
                g.DoAnswer(false);
            }
            else
            {
                
                g.DoAnswer(true);
            }
        }

        private void bu_About_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Добро пожаловать в игру Отгадай цвет."+"\nВам нужно кликать по прямоугольникам с правильным цветом."+"\n Для того чтобы начать играть,вам необходимо сгенерировать новое поле.");
        }

        private void timer_Tick(object sender, EventArgs e)
        {
  
        }
    }
}
