﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exam_app
{
    public partial class Game_exit : Form
    {
        public Game_exit()
        {
            InitializeComponent();
        }

        private void bu_Res_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        private void Game_exit_Load(object sender, EventArgs e)
        {

        }
    }
}
