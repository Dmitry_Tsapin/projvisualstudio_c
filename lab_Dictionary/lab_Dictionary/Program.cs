﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, string> x = new Dictionary<int, string>(5);
            x.Add(1, "Брянск");
            x.Add(3, "Тула");
            x.Add(2, "Москва");
            x.Add(4, "Питер");
            x.Add(5, "Сочи");
            //
            foreach(KeyValuePair<int,string> keyValue in x)
            {
                Console.WriteLine(keyValue.Key + "-" + keyValue.Value);

            }
            //
            string country = x[4];
            x[4] = "Germany";
            x.Remove(2);
            x.Add(7, "Spain");
            Console.WriteLine("------------------------");
            foreach (KeyValuePair<int, string> keyValue in x)
            {
               
                Console.WriteLine(keyValue.Key + "-" + keyValue.Value);

            }
            x.Remove(5);
            x.Add(8, "Italy");
            Console.WriteLine("------------------------");
            foreach (KeyValuePair<int, string> keyValue in x)
            {

                Console.WriteLine(keyValue.Key + "-" + keyValue.Value);

            }
        }
    }
}
