﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using lab_My_Math;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_My_Math.Tests
{
    [TestClass()]
    public class MyMathTests
    {
        [TestMethod()]
        public void AddTest()
        {
            int a = 3;
            int b = 2;
            int c = 5;
            //
            int rr = MyMath.Add(a, b);
            //
            Assert.AreEqual(rr, c);
        }


        [TestMethod()]
        public void AddBTest()
        {
            int a = -3;
            int b = -1;
            int c = -4;
            //
            int rr = MyMath.Add(a, b);
            //
            Assert.AreEqual(rr, c);
        }

        [TestMethod()]
        public void Add2Test()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void SubTest()
        {
            int a = 4;
            int b = 2;
            int c = 2;
            //
            int rr = MyMath.Sub(a, b);
            //
            Assert.AreEqual(rr, c);
        }

        [TestMethod()]
        public void MultiTest()
        {
            int a = 3;
            int b = 3;
            int c = 9;
            //
            int rr = MyMath.Multi(a, b);
            //
            Assert.AreEqual(rr, c);
        }
    }
}