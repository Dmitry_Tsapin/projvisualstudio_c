﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Parse
{
    class Program
    {  // Преобразование числовых данных,введеных с консоли
        static void Main(string[] args)
        {   // Первый способ
            int x;
            Console.Write("(1) x = ");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("(1) x = {0}", x);
           
            // Второй способ
            Console.Write("(2) x = ");
            int.TryParse(Console.ReadLine(), out x);
            Console.WriteLine("(2) x = {0}", x);
            
            // Третий способ
            Console.Write("(3) x = ");
             x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("(3) x = {0}", x);
            
            // Явное преобразование (четвертый способ)
            double d = 124.4;
            d = x;
            x = (int)d;
            Console.WriteLine(" d = {0}", d);
            Console.WriteLine(" x = {0}", x);

        }
    }
}
