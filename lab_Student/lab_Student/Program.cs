﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Student
{
    class Program
    {
        static void Main(string[] args)
        {
            Student x = new Student();
            x.ChangeAge += X_ChangeAge;
            x.ChangePassport += X_ChangePassport;
            x.Name = "Иван";
            x.SurName = "Иванов";
            x.Otchestvo = "Иванович";
            x.Age = 16;
            x.Passport = 4512_841291;
            Console.WriteLine(x.GetFullName());
            x.Age = 18;
            x.Passport = 4519_555291;
            Console.WriteLine(x.GetFullName());
            x.Age = 17;
            Console.WriteLine(x.GetFullName());

            Console.WriteLine("-------------------");

            // Второй способ
            Student student = new Student() { Name = "Михаил", Otchestvo = "Дмитриевич", SurName = "Дегтярев", Passport = 4612_5555, Age = 20 };
            Console.WriteLine(student.GetFullName());
        }

        private static void X_ChangePassport(object sender, EventArgs e)
        {
            Console.WriteLine("Изменились паспортные данные");
        }

        public static void X_ChangeAge(object sender, EventArgs e)
        {
            Console.WriteLine("Изменился возраст");
        }
    }
    
}

