﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_Student
{
    class Student
    {
        public event EventHandler ChangeAge;

        private int _age;

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value > _age)
                {
                    _age = value;
                    if (ChangeAge != null)
                        ChangeAge(this, EventArgs.Empty);
                }
                else
                    Console.WriteLine("Ошибка: Возраст в меньшую сторону изменить невозмонжно.Текущий возраст студента:" + Age);
            }
        }


        public event EventHandler ChangePassport;

        private long _passport;

        public long Passport
        {
            get
            {
                return _passport;
            }
            set
            {
                if (value > _passport)
                {
                    _passport = value;
                    if (ChangePassport != null)
                        ChangePassport(this, EventArgs.Empty);
                }
                
            }
        }
        public string Name { get; set; }
        public string Otchestvo { get; set; }
        public string SurName { get; set; }
        public string GetFullName()
        {
            return Name + " " + Otchestvo + " " + SurName + " "  + Passport  + " (" + Age + ")";
        }

    }
}
